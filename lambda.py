# -*- coding: utf-8 -*-

import sys, os
try:
  import readline
except ImportError:
  import pyreadline as readline
import shlex
import json
import configparser
import signal
import argparse
import zipfile
import boto3
import datetime
from time import strptime, strftime, mktime, gmtime

from complete import Completer

temporaryPath = '__temporary'
log_client = boto3.client('logs')

# Exit current process
def FuncExit(args):
    print('Finishing job...')
    exit()

# Print help contents
def PrintHelp(args):
    print('help: show help')
    print('exit: exit')
    print('update [lambda function name]: update lambda source code')
    print('invoke [lambda function name]: invoke lambda')
    print('region [region name]: get or set current region')
    print('payload [payload json file name]: get or set current payload json')
    print('profile [profile name]: get or set current profile name')
    print('log [lambda function name]: view logs')
    print('enablelog [lambda function name]: enable logs')
    print('disablelog [lambda function name]: disable logs')
    print('starttime [n minutes]: set log start time (Ex: 3 -> 3 minutes ago)')

# Print Invalid error message
def PrintInvalid():
    print('Invalid command or arguments.')
    print('To get help, enter `help`.')

# Print exception info
def PrintException(e):
    exc_type, exc_obj, exc_tb = sys.exc_info()
    fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
    print(exc_type, fname, exc_tb.tb_lineno)

def datetime_converter(datetime_string):
    # (1) Convert to datetime format
    target_timestamp = strptime(datetime_string, '%Y-%m-%d %H:%M:%S')

    # (2) mktime creates epoch time from the local time
    mktime_epoch = mktime(target_timestamp)
    #print(int(mktime_epoch)) # convert to integer to remove decimal
    return int(mktime_epoch)

    # (3) gmtime to convert epoch time into UTC time object
    #epoch_to_timestamp = strftime('%Y-%m-%d %H:%M:%S', gmtime(mktime_epoch))
    #print(epoch_to_timestamp)
    #print('the end')
    
def cutLastSlash(s):
    l = len(s)
    if s[l-1] == '/' or s[l-1] == '\\':
        return s[0:l-1]
    else:
        return s


def makeTemporaryDir():
    if not os.path.exists(temporaryPath):
        os.makedirs(temporaryPath)

def removeOldFile(s):
    if os.path.exists('./'+s):
        os.remove('./'+s)

def updateLambda(args):
    global region
    if len(args) == 2:
        try:
            s = cutLastSlash(args[1])
            print('Updae following lambda: '+s)
            makeTemporaryDir()

            zName = s+'.zip'
            removeOldFile(zName)

            targetZfile = zipfile.ZipFile(zName, 'w')

            print("Start compressing")
            for folder, subfolders, files in os.walk(s):
                for file in files:
                    targetZfile.write(os.path.join(folder, file), os.path.relpath(os.path.join(folder,file), s), compress_type = zipfile.ZIP_DEFLATED)
            targetZfile.close()
            print("End compressing")

            tempPath = temporaryPath+'/'+zName

            removeOldFile(tempPath)
            os.rename('./'+zName, tempPath)

            aws_cmd = 'aws lambda update-function-code --profile '+profile+' --region '+region+' --function-name '+s+' --zip-file fileb://'+tempPath
            print(aws_cmd)
            os.system(aws_cmd)
        except Exception as e:
            PrintException(e)
            return False
    else:
        print("Invalid argument")

    return True

def invokeLambda(args):
    global region, payload, profile
    if len(args) == 2:
        try:
            s = cutLastSlash(args[1])
            print('Invoke following lambda: '+s)

            makeTemporaryDir()

            aws_cmd = ''

            if payload != None:
                aws_cmd = 'aws lambda invoke --profile '+profile+' --invocation-type RequestResponse --function-name '+s+' --region '+region+' --payload file://'+payload+' '+temporaryPath+'/output.txt'
            else:
                aws_cmd = 'aws lambda invoke --profile '+profile+' --invocation-type RequestResponse --function-name '+s+' --region '+region+' --payload \'{"test":"test value"}\' '+temporaryPath+'/output.txt'
            print(aws_cmd)
            os.system(aws_cmd)

            print('<<<<< Response from lambda >>>>>')
            output_json = {}
            with open(temporaryPath+'/output.txt') as data_file:    
                output_json = json.load(data_file, encoding="utf-8")
            print(output_json)
        except Exception as e:
            PrintException(e)
            return False
    else:
        print("Invalid argument")

    return True

def readLog(args):
    global region, logTime
    if len(args) == 2:
        try:
            s = cutLastSlash(args[1])
            print('View logs of: '+s)

            current_time = datetime.datetime.now(datetime.timezone.utc)
            minutes = datetime.timedelta(minutes = logTime)
            start_time = current_time - minutes
            #print(start_time)

            utc = int(mktime(strptime(start_time.strftime('%Y-%m-%d %H:%M:%S'), '%Y-%m-%d %H:%M:%S')))

            response = log_client.describe_log_streams(
                logGroupName='/aws/lambda/'+s,
                orderBy='LastEventTime',
                descending=True,
                limit=1
                )
            logStreamName = response['logStreams'][0]['logStreamName']

            logs = log_client.get_log_events(
                logGroupName='/aws/lambda/'+s,
                logStreamName=logStreamName,
                startTime=utc,
                startFromHead=True
                )
            
            #print('start print')
            for ev in logs['events']:
                #print(ev)
                #print(datetime.datetime.strptime(int(ev['timestamp']), "%Y-%m-%dT%H:%M:%S.%fZ"))
                print(str(ev['timestamp'])+': '+ev['message'])

        except Exception as e:
            PrintException(e)
            return False
    else:
        print("Invalid argument")

    return True

def enableLog(args):
    global region
    if len(args) == 2:
        try:
            s = cutLastSlash(args[1])
            print('Enable log of following lambda: '+s)
            aws_cmd = 'aws lambda update-function-configuration --profile '+profile+' --region '+region+' --function-name '+s+' --environment "Variables={ENABLE_LOG=1}"'
            print(aws_cmd)
            os.system(aws_cmd)
        except Exception as e:
            PrintException(e)
            return False
    else:
        print("Invalid argument")

    return True

def disableLog(args):
    global region
    if len(args) == 2:
        try:
            s = cutLastSlash(args[1])
            print('Enable log of following lambda: '+s)
            aws_cmd = 'aws lambda update-function-configuration --profile '+profile+' --region '+region+' --function-name '+s+' --environment "Variables={ENABLE_LOG=0}"'
            print(aws_cmd)
            os.system(aws_cmd)
        except Exception as e:
            PrintException(e)
            return False
    else:
        print("Invalid argument")

    return True

def setLogTime(args):
    global logTime
    if len(args) == 2:
        logTime = int(args[1])
    else:
        print(str(logTime)+' minute(s)')

def changeRegion(args):
    global region
    if len(args) == 1:
        print("Current region: "+region)
    elif len(args) == 2:
        region = args[1]
        print("Region name changed to: "+region)

    return True

def changeProfile(args):
    global profile
    if len(args) == 1:
        print("Current profile: "+profile)
    elif len(args) == 2:
        profile = args[1]
        print("Profile name changed to: "+profile)

    return True

def setPayload(args):
    global payload
    data = {}
    if payload == None:
        data = {}
    else:
        with open(payload) as data_file:    
            data = json.load(data_file, encoding="utf-8")

    if len(args) == 1:
        print("Current payload: "+json.dumps(data))
    elif len(args) == 2:
        payload = args[1]
        with open(payload) as data_file:    
            data = json.load(data_file, encoding="utf-8")
        print("Payload changed to: "+json.dumps(data))

    return True

# Fix Python 2.x.
try: input = raw_input
except NameError: pass

# signal handler
def sighandler(signum, frame):
    print('Stop by Ctrl+C')
    FuncExit([''])
signal.signal(signal.SIGINT, sighandler)

shellFuncDict = {
    'help': PrintHelp,
    'exit': FuncExit,
    'update': updateLambda,
    'invoke': invokeLambda,
    'region': changeRegion,
    'payload': setPayload,
    'profile': changeProfile,
    'log': readLog,
    'enablelog': enableLog,
    'disablelog': disableLog,
    'starttime': setLogTime
    }

# Read in command-line parameters
parser = argparse.ArgumentParser()
parser.add_argument("-r", "--region", action="store", required=False, dest="region", default='ap-northeast-2', help="region name")
args = parser.parse_args()
region = args.region
payload = None
profile = "default"
logTime = 1

# Main function
if __name__ == "__main__":
    comp = Completer()
    # we want to treat '/' as part of a word, so override the delimiters
    readline.set_completer_delims(' \t\n;')
    readline.parse_and_bind("tab: complete")
    readline.set_completer(comp.complete)
    cmds = []

    for k, v in shellFuncDict.items():
        cmds.append(k)

    comp.set_command(cmds)

    # Print header contents
    print('To get help, enter `help`.')

    try:
        isExit = False
        while isExit == False:
            # Get commands
            try:
                cmd = input("> ")
            except:
                exit()

            print(cmd)
            args = shlex.split(cmd, posix=False)

            if len(args) == 0:
                continue

            try:
                # run command
                shellFuncDict[args[0]](args)

                if args[0] == 'exit':
                    isExit = True
            except Exception as e:
                if args[0] in shellFuncDict:
                    PrintException(e)
                else:
                    print('Unknown command or invalid arguments')

    except Exception as e:
        PrintException(e)
