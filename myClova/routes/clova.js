const express = require('express');
const cekRequest = require('../index.js');
const { json } = require('../http');
const router = express.Router();

router.post('/', json(cekRequest));

module.exports = router;