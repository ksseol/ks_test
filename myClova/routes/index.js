const express = require('express');
const clova = require('../index.js');
const router = express.Router();

router.post(`/clova`, clova);

module.exports = router;
